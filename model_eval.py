import numpy as np
import h5py
import keras
import os

from resolution_scaler import basewidth, baseheight

train_idxs = np.load('pydata/train_idxs.npy')
val_idxs = np.load('pydata/val_idxs.npy')
test_idxs = np.load('pydata/test_idxs.npy')

f = h5py.File('pydata/obj_images_120x160_gray.hdf5', 'r')
l = h5py.File('pydata/obj_labels.hdf5', 'r')

images = f.get('images')
labels = l.get('labels')

label = {0: 'left', 1: 'none', 2: 'right'}

if __name__ == '__main__':
    model = keras.models.load_model('saved_models/model_bs128_acc96_20201007_2136.h5')

    x = []
    y = []
    for i in test_idxs:
        x.append(images[i])
        y.append(labels[i])

    test_imgs = (( np.array(x, dtype=np.float16) / 127.5 ) - 1)
    test_lbls = np.array(y)

    true = []
    for lb in test_lbls:
        true.append(label[np.argmax(lb)])

    predictions = []
    for img in test_imgs:
        predictions.append(model.predict(img.reshape(1,baseheight,basewidth,1)))
    predictions = np.array(predictions)

    predicted = []
    for pred in predictions:
        predicted.append(label[np.argmax(pred)])

    correct = 0
    total = 0
    failures_idxs = []
    for i in range(len(test_imgs)):
        total += 1
        if true[i] == predicted[i]:
            correct += 1
        else:
            failures_idxs.append(test_idxs[i])
            print(test_idxs[i])

    failures_idxs = np.array(failures_idxs)
    np.save('failures_idxs.npy', failures_idxs)
    print('correct', correct)
    print('total', total)
    accuracy = np.float16(correct)/np.float16(total) * 100
    print('Accuracy: ', accuracy, '%')

'''
# TO LOAD IMAGE FROM HDF5 AND DISPLAY
from PIL import Image
import h5py

f = h5py.File('pydata/images.hdf5', 'r')
images = f.get('images')
idx = 1000 # set image index here 
img = images[idx]
img = img.astype('uint8')
img = img.reshape((height, width, channels)) # invert line 40 of pydata_generator.py
img = Image.fromarray(img)
img.show()

'''
