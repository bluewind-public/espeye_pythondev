import PIL
from PIL import Image, ImageOps
import os
import cv2
import numpy as np

TO_GRAYSCALE = True

basewidth = 160
baseheight = 120

input_data_path = 'esp_gesture_dataset/obj_augmented/'
if TO_GRAYSCALE:
    output_data_path = 'esp_gesture_dataset/obj_augmented' + '_' + str(baseheight) + 'x' + str(basewidth) + '_gray'
else:
    output_data_path = 'esp_gesture_dataset/obj_augmented' + '_' + str(baseheight) + 'x' + str(basewidth)

bg_colour=(255, 255, 255)
bg = Image.new("RGBA", (basewidth,baseheight), bg_colour + (255,)) # PIL.Image.new requires (width, height)

if __name__ == '__main__':
    for cat in os.listdir(input_data_path):
        if not os.path.isdir(output_data_path + '/' + cat):
            os.mkdir(output_data_path + '/' + cat)
        for file in os.listdir(input_data_path + cat):
            file_in_path = input_data_path + cat + '/' + file
            file_out_path = output_data_path + '/' + cat + '/' + file
            if TO_GRAYSCALE:
                img = Image.open(file_in_path)
                alpha = img.convert('RGBA').split()[-1]
                bg.paste(img, mask=alpha)
                img = bg.convert('L')
            else:
                img = Image.open(file_in_path)
            img = img.resize((baseheight, basewidth), PIL.Image.ANTIALIAS)
            img.save(file_out_path)
