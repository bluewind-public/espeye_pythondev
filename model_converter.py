import tensorflow as tf
import keras
import h5py
import numpy as np

from resolution_scaler import basewidth, baseheight

tfmodel_path = 'saved_models/model_bs128_acc94_20201008_1420.h5'
tfmodel = keras.models.load_model(tfmodel_path)

def representative_dataset_generator():
    f = h5py.File('pydata/obj_images_' + str(baseheight) + 'x' + str(basewidth) + '_gray.hdf5', 'r')
    images = f.get('images')
    test_idx = np.load('pydata/test_idxs.npy')
    for i in test_idx[:320]:
        print(i)
        yield [np.expand_dims((np.array(images[i], dtype=np.float32) / 127.5) - 1, axis = 0)]

if __name__ == '__main__':
    converter = tf.lite.TFLiteConverter.from_keras_model(tfmodel)
    tflite_model = converter.convert()
    open('saved_models/gest_rec_model.tflite', 'wb').write(tflite_model)

    converter.optimizations = [tf.lite.Optimize.DEFAULT]
    converter.target_spec.supported_ops = [tf.lite.OpsSet.TFLITE_BUILTINS_INT8]
    converter.representative_dataset = representative_dataset_generator
    converter.inference_input_type = tf.int8
    converter.inference_output_type = tf.int8
    tflite_quant_model = converter.convert()
    open('saved_models/gest_rec_quantized_model.tflite', 'wb').write(tflite_quant_model)