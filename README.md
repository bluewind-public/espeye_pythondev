# Bluewind Edge AI academy

## Workshop 2: image classification on esp32

### About

During the second day of the Edge AI Academy offered by Bluewind we showed how to develop an image classifier with python.  
Moreover we learnt how to deploy it to the ESP-EYE development board.  

In this repository you will find the code to:  
1. Resize pictures using OpenCV and PIL
2. Augment your dataset
3. Build and train a Neural Nework to classify images
4. Convert the pretrained model to a tflite quantized model

### Requirements

1. Python 3
2. pip
3. Required libraries at _requirements.txt_

Create a python virtual environment and install requirements running the following:  
> python -m venv --system-site-packages my-env
> source my-env/bin/activate
> pip install requirements.txt

### Usage

You should build your base dataset using the [esp-who camera_web_server app](https://github.com/espressif/esp-who/tree/master/examples/single_chip/camera_web_server).
30 images per class should be enough.
A sample dataset is provided for testing.

Afterwards you will:  

- Rescale your picture using *resolution_scaler.py*
- Augment your dataset using *data_augmentation.py*
- Generate your H5DF dataset using *pydata_generator.py*
- train, evaluate and save your classifier with *classifier.py*
- convert your model with *model_converter.py*
- evaluate your converted model with *tflite_evaluate.py*

Finally you want to convert yout tflite quantized model to a .cc file by running:  
> xxd -i MyQuantizedModel.tflite > MyQuantizedModel.cc

### Credits and support

For any inquiry contact:
 
- Pietro Montino, Bluewind
- Davide Ceccon, Bluewind

