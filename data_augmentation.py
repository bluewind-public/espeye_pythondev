import keras
import numpy as np
import matplotlib.pyplot as plt
import os
import cv2
import random
from resolution_scaler import baseheight, basewidth

DEPTH = 3

class Data_augmentation:
    def __init__(self, path, image_name):
        '''
        Import image
        :param path: Path to the image
        :param image_name: image name
        '''
        self.path = path
        self.name = image_name
        self.image = np.int16(cv2.imread(path + '/' + image_name))

    def rotate(self, image, angle=90, scale=1.0):
        '''
        Rotate the image
        :param image: image to be processed
        :param angle: Rotation angle in degrees. Positive values mean counter-clockwise rotation (the coordinate origin is assumed to be the top-left corner).
        :param scale: Isotropic scale factor.
        '''
        w = image.shape[1]
        h = image.shape[0]
        # rotate matrix
        M = cv2.getRotationMatrix2D((w / 2, h / 2), angle, scale)
        # rotate
        image = cv2.warpAffine(image, M, (w, h))
        return image

    def flip(self, image, vflip=False, hflip=False):
        '''
        Flip the image
        :param image: image to be processed
        :param vflip: whether to flip the image vertically
        :param hflip: whether to flip the image horizontally
        '''
        if hflip or vflip:
            if hflip and vflip:
                c = -1
            else:
                c = 0 if vflip else 1
            image = cv2.flip(image, flipCode=c)
        return image

    def add_GaussianNoise(self, image):
        # ADDING NOISE
        noise = np.random.randint(-20, high=20, size=(baseheight, basewidth, DEPTH), dtype='int8')
        image = image + noise
        return image

    def image_augment(self, save_path):
        '''
        Create the new image with imge augmentation
        :param path: the path to store the new image
        '''
        img = self.image.copy()
        #img_flip = self.flip(img, vflip=False, hflip=False)
        #print('flipped')
        name_int = self.name[:len(self.name) - 4]
        for angle in range(-70,70,2):
            img_rot = self.rotate(img, angle=angle)
            img_gaussian = self.add_GaussianNoise(img_rot)
            cv2.imwrite(save_path + '/' + '%s' % str(name_int) + '_rot' + str(angle) + '.jpg', img_rot)
            cv2.imwrite(save_path + '/' + '%s' % str(name_int) + str(angle) + '_GaussianNoise.jpg', img_gaussian)

def main(file_dir, output_path):
    for dir in os.listdir(file_dir):
        class_path = file_dir + dir
        augmented_path = output_path + dir
        print(class_path)
        print(augmented_path)
        for file in os.listdir(class_path):
            print(file)
            raw_image = Data_augmentation(class_path, file)
            raw_image.image_augment(augmented_path)

file_dir = './esp_gesture_dataset/object/'
output_path = './esp_gesture_dataset/obj_augmented/'
main(file_dir, output_path)


