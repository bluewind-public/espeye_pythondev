import tensorflow as tf
import numpy as np
import h5py
import keras
import os
import sys
from resolution_scaler import basewidth, baseheight

np.set_printoptions(threshold=sys.maxsize)

train_idxs = np.load('pydata/train_idxs.npy')
val_idxs = np.load('pydata/val_idxs.npy')
test_idxs = np.load('pydata/test_idxs.npy')

f = h5py.File('pydata/obj_images_120x160_gray.hdf5', 'r')
l = h5py.File('pydata/obj_labels.hdf5', 'r')

images = f.get('images')
labels = l.get('labels')

label = {0: 'usb',1: 'none',2: 'ieee'}

QUANT_MODEL = True

if __name__ == '__main__':
    # Load the TFLite model and allocate tensors.
    interpreter = tf.lite.Interpreter(model_path="/home/pietromo/Projects/esp_gesture_recognition/saved_models/gest_rec_quantized_model.tflite")
    interpreter.allocate_tensors()

    # Get input and output tensors.
    input_details = interpreter.get_input_details()
    output_details = interpreter.get_output_details()

    x = []
    y = []
    for i in test_idxs:
        x.append(images[i])
        y.append(labels[i])

    if QUANT_MODEL:
        test_imgs = np.array(x, dtype=np.int64)
        test_imgs = test_imgs - 128
        test_imgs = test_imgs.astype(np.int8)
    else:
        test_imgs = (( np.array(x, dtype=np.float32) / 127.5 ) - 1)
    test_lbls = np.array(y)

    true = []
    for lb in test_lbls:
        true.append(label[np.argmax(lb)])

    predictions = []
    for img in test_imgs:
        input_data = img.reshape(1, baseheight, basewidth, 1)
        interpreter.set_tensor(input_details[0]['index'], input_data)
        interpreter.invoke()
        output_data = interpreter.get_tensor(output_details[0]['index'])
        predictions.append(output_data)
        print(output_data)
    predictions = np.array(predictions)

    predicted = []
    for pred in predictions:
        predicted.append(label[np.argmax(pred)])

    test_imgs = (( np.array(x, dtype=np.float32) / 127.5 ) - 1)
    test_lbls = np.array(y)

    correct = 0
    total = 0
    failures_idxs = []
    for i in range(len(test_imgs)):
        total += 1
        if true[i] == predicted[i]:
            correct += 1
        else:
            failures_idxs.append(test_idxs[i])
            #print(test_idxs[i])

    failures_idxs = np.array(failures_idxs)
    np.save('failures_idxs.npy', failures_idxs)
    print('correct', correct)
    print('total', total)
    accuracy = np.float16(correct)/np.float16(total) * 100
    print('Accuracy: ', accuracy, '%')