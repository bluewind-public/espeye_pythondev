import keras
from keras.models import Sequential
from keras.layers import Conv2D, MaxPooling2D, DepthwiseConv2D, AvgPool2D, SeparableConv2D, Input
from keras.layers import Activation, Dropout, Flatten, Dense, BatchNormalization, Reshape
import numpy as np
import sklearn
from sklearn.utils import shuffle
import h5py
import random
from datetime import datetime
import os
import time
print('hello')
from resolution_scaler import basewidth, baseheight, TO_GRAYSCALE
NUMBER_OF_SAMPLES = 0

import matplotlib.pyplot as plt

if __name__ == '__main__':
    if TO_GRAYSCALE:
        f = h5py.File('pydata/obj_images_' + str(baseheight) + 'x' + str(basewidth) + '_gray.hdf5', 'r')
    else:
        f = h5py.File('pydata/obj_images_' + str(baseheight) + 'x' + str(basewidth) + '.hdf5', 'r')

    l = h5py.File('pydata/obj_labels.hdf5', 'r')

    images = f.get('images')
    labels = l.get('labels')
    NUMBER_OF_SAMPLES = images.shape[0]
    NUMBER_OF_TRAIN_SAMPLES = int(NUMBER_OF_SAMPLES* 0.6)
    NUMBER_OF_VAL_SAMPLES = int(( NUMBER_OF_SAMPLES - NUMBER_OF_TRAIN_SAMPLES )  / 2 )
    NUMBER_OF_TEST_SAMPLE = NUMBER_OF_SAMPLES - NUMBER_OF_TRAIN_SAMPLES - NUMBER_OF_VAL_SAMPLES

    EPOCHS = 200
    BATCH_SIZE = 128
    ITERATIONS = int(NUMBER_OF_TRAIN_SAMPLES / BATCH_SIZE)
    indexes = np.array(range(NUMBER_OF_SAMPLES))
    np.random.shuffle(indexes)
    train_indexes = indexes[:NUMBER_OF_TRAIN_SAMPLES]
    val_indexes = indexes[NUMBER_OF_TRAIN_SAMPLES:NUMBER_OF_TRAIN_SAMPLES+NUMBER_OF_VAL_SAMPLES]
    test_indexes = indexes[-NUMBER_OF_TEST_SAMPLE:]

    np.save('pydata/train_idxs.npy', train_indexes)
    np.save('pydata/val_idxs.npy', val_indexes)
    np.save('pydata/test_idxs.npy', test_indexes)

    if not os.path.isdir('saved_models'):
        os.mkdir('saved_models')

    import tensorflow as tf
    tf.keras.backend.clear_session()

    if TO_GRAYSCALE:
        channels = 1
    else:
        channels = 3

    dropout = 0.2
    depth_multiplier = 1
    BATCH_NORM = False
    USE_SEP_CONV = False
    padding = 'valid'
    def get_model():
        model = Sequential()
        if USE_SEP_CONV:
            model.add(SeparableConv2D(4, (3, 3), depth_multiplier=depth_multiplier, input_shape=(baseheight, basewidth, channels), padding=padding))
        else:
            model.add(Conv2D(4, (3, 3), input_shape=(baseheight, basewidth, channels), padding=padding))
        model.add(Activation('relu'))
        if BATCH_NORM:
            model.add(BatchNormalization())
        model.add(MaxPooling2D(pool_size=(4, 4)))
        model.add(Dropout(dropout))

        if USE_SEP_CONV:
            model.add(SeparableConv2D(4, (3, 3), depth_multiplier=depth_multiplier, padding=padding))
        else:
            model.add(Conv2D(4, (3, 3), padding=padding))
        model.add(Activation('relu'))
        if BATCH_NORM:
            model.add(BatchNormalization())
        model.add(MaxPooling2D(pool_size=(2, 2)))
        model.add(Dropout(dropout))

        if USE_SEP_CONV:
            model.add(SeparableConv2D(4, (3, 3), depth_multiplier=depth_multiplier, padding=padding))
        else:
            model.add(Conv2D(4, (3, 3), padding=padding))
        model.add(Activation('relu'))
        if BATCH_NORM:
            model.add(BatchNormalization())
        model.add(MaxPooling2D(pool_size=(2, 2)))
        model.add(Dropout(dropout))

        if USE_SEP_CONV:
            model.add(SeparableConv2D(4, (3, 3), depth_multiplier=depth_multiplier, padding=padding))
        else:
            model.add(Conv2D(4, (3, 3), padding=padding))
        model.add(Activation('relu'))
        if BATCH_NORM:
            model.add(BatchNormalization())
        model.add(MaxPooling2D(pool_size=(2, 2)))
        model.add(Dropout(dropout))

        model.add(Flatten())  # this converts our 3D feature maps to 1D feature vectors
        model.add(Dense(8))
        model.add(Activation('relu'))
        model.add(Dropout(dropout))
        model.add(Dense(3))
        model.add(Activation('softmax'))

        model.summary()
        return model

    def shuffle_index():
        return random.shuffle(indexes)

    opt = keras.optimizers.Adam(learning_rate=0.01, beta_1 = 0.95)
    #opt = keras.optimizers.Adam()
    def model_compile(model):
        return model.compile(loss='categorical_crossentropy',
                  optimizer=opt,
                  metrics=['accuracy'])

    def get_next_train_batch(iter):
        x = []
        y= []
        for i in range(iter*BATCH_SIZE,(iter+1)*BATCH_SIZE):
            x.append(images[train_indexes[i]])
            y.append(labels[train_indexes[i]])
        return (np.array(x, dtype=np.float32) / 127.5) - 1, np.array(y)

    def get_next_val_batch(iter):
        x = []
        y= []
        for i in range(iter*BATCH_SIZE,(iter+1)*BATCH_SIZE):
            x.append(images[val_indexes[i]])
            y.append(labels[val_indexes[i]])
        return (np.array(x, dtype=np.float32) / 127.5) - 1, np.array(y)

    def validate_model(model):
        val_loss = 0.0  #loss
        val_acc = 0.0   #acc
        for iter in range(int(NUMBER_OF_VAL_SAMPLES/BATCH_SIZE)):
            x, y = get_next_val_batch(iter)
            new_metrics = model.test_on_batch(x, y)
            val_loss += new_metrics[0]
            val_acc += new_metrics[1]
        val_loss = val_loss / np.float(NUMBER_OF_VAL_SAMPLES/BATCH_SIZE)
        val_acc = val_acc / np.float(NUMBER_OF_VAL_SAMPLES/BATCH_SIZE)
        return [val_loss, val_acc]

    model = get_model()
    model_compile(model)
    print('---------------')
    print('model compiled!')

    history = []
    TOP_VAL_SCORE = 0
    for epoch in range(EPOCHS):
        start = time.time()
        train_acc = 0
        train_loss = 0
        for iter in range(ITERATIONS):
            x, y = get_next_train_batch(iter)
            train_metrics  = model.train_on_batch(x, y)
            train_loss += train_metrics[0]
            train_acc += train_metrics[1]
            print(iter, ": train_loss", train_metrics[0], 'train_acc', train_metrics[1])
        val_metrics = validate_model(model)
        history.append([train_loss/ITERATIONS, train_acc/ITERATIONS, val_metrics[0] , val_metrics[1]])
        print("EPOCH: ", epoch, "val_loss", val_metrics[0], 'val_acc', val_metrics[1])
        end = time.time()
        print("time for epoch: ", (end-start) / 60 , 'minutes')
        if val_metrics[1] > TOP_VAL_SCORE:
            TOP_VAL_SCORE = val_metrics[1]
            model_name = 'saved_models/model_bs{}_acc{}_{}.h5'
            model.save(model_name.format(BATCH_SIZE, int(100*val_metrics[1]), datetime.today().strftime('%Y%m%d_%H%M')))

    history = np.array(history)

    fig = plt.figure()
    ax = fig.add_subplot(1, 1, 1)
    ax.plot(range(EPOCHS), history[:,0], label='train_loss')
    ax.plot(range(EPOCHS), history[:,2], label='val_loss')
    ax.legend()
    plt_name = 'plots/loss_e{}_bs{}_{}.png'
    fig.savefig(plt_name.format(EPOCHS, BATCH_SIZE, datetime.today().strftime('%Y%m%d')))
    plt.close(fig)

    fig = plt.figure()
    ax = fig.add_subplot(1, 1, 1)
    ax.plot(range(EPOCHS), history[:,1], label='train_acc')
    ax.plot(range(EPOCHS), history[:,3], label='val_acc')
    ax.legend()
    plt_name = 'plots/acc_e{}_bs{}_{}.png'
    fig.savefig(plt_name.format(EPOCHS, BATCH_SIZE, datetime.today().strftime('%Y%m%d')))
    plt.close(fig)
