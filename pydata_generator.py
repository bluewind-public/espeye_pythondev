import numpy as np
import os
import cv2
import h5py

from resolution_scaler import basewidth, baseheight, TO_GRAYSCALE

if TO_GRAYSCALE:
    dataset_path = 'esp_gesture_dataset/obj_augmented' + '_' + str(baseheight) + 'x' + str(basewidth) + '_gray/'
    channels = 1
    color =  cv2.IMREAD_GRAYSCALE
else:
    dataset_path = 'esp_gesture_dataset/obj_augmented' + '_' + str(baseheight) + 'x' + str(basewidth) + '/'
    channels = 3
    color = cv2.COLOR_BGR2RGB

NUMBER_OF_SAMPLES = 0

USE_HDF5    =    True

#label = {'none': [0,1,0], 'right': [0,0,1], 'left': [1,0,0]}
label = {'usb': [1,0,0], 'none': [0,1,0], 'ieee': [0,0,1]}

if __name__ == '__main__':
    if not os.path.exists('pydata'):
        os.makedirs('pydata')

    for cat in os.listdir(dataset_path):
        NUMBER_OF_SAMPLES += len(os.listdir(dataset_path + '/' + cat))

    if USE_HDF5:
        if os.path.isfile('pydata/obj_images_' + str(baseheight) + 'x' + str(basewidth) + '_gray.hdf5'):
            os.remove('pydata/obj_images_' + str(baseheight) + 'x' + str(basewidth) + '_gray.hdf5')
        if os.path.isfile('pydata/obj_labels.hdf5'):
            os.remove('pydata/obj_labels.hdf5')
        f = h5py.File('pydata/obj_images_' + str(baseheight) + 'x' + str(basewidth) + '_gray.hdf5', 'a')
        l = h5py.File('pydata/obj_labels.hdf5', 'a')
        images = f.create_dataset("images", (NUMBER_OF_SAMPLES, baseheight, basewidth, channels), dtype='uint8') # WATCH OUT: dtype here is important
        labels = l.create_dataset("labels", (NUMBER_OF_SAMPLES, 3), dtype='int8')
    else:
        images = []
        labels = []

    if USE_HDF5:
        i=0
        for cat in os.listdir(dataset_path):
            for file in os.listdir(dataset_path + cat):
                #img_bgr = cv2.imread(dataset_path + cat + '/' + file)
                #img = cv2.cvtColor(img_bgr, cv2.COLOR_BGR2RGB)
                img = cv2.imread(dataset_path + cat + '/' + file, color)
                images[i,:,:,:] = np.array(img).reshape((1, baseheight, basewidth, channels))
                labels[i,:] = np.array(label[cat]).reshape((1,3))
                i+=1
                print(i)
        f.close()
        l.close()

    else:
        i=0
        for cat in os.listdir(dataset_path):
            for file in os.listdir(dataset_path + cat):
                print(dataset_path + cat + '/' + file)
                img_bgr = cv2.imread(dataset_path + cat + '/'  + file)
                img = cv2.cvtColor(img_bgr, cv2.COLOR_BGR2RGB)
                images.append(img)
                labels.append(label[cat])
                i+=1
                print(i)
        images = np.array(images)
        labels = np.array(labels)
        np.save('pydata/images_' + str(baseheight) + 'x' + str(basewidth) + '.npy', images)
        np.save('pydata/labels.npy', labels)
